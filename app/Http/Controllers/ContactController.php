<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Mail\Contact;

class ContactController extends Controller
{ 
    public function create()
    {
        return view('Accuiel');
      
    }
 
    public function store(ContactRequest $request)
    {  
        Mail::send(new Contact($request->message,$request->email,$request->subject,$request->name)) ;
       
        return view('Accuiel');

    }
       
    
    }