<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Contact extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $messages;
    public $subject;
    public $email;
    public  $name;
    public function __construct($messages,$email,$subject,$name)
    {
        $this->messages = $messages; 
        $this->subject = $subject; 
        $this->email = $email; 
        $this->name = $name; 
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {          
        return $this ->from('stneurotn@gmail.com','stneurotn@gmail.com')
        ->to('stneurotn@gmail.com')->cc($this->email)
       ->subject($this->subject)
       ->view('email.contact');

}
}
