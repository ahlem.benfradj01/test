<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <style>
  
  </style>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contact Us form</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
    
<nav class="nav" id="navbar">
        <div class="container">
                <a href="\Accuiel"><img src="storage\img\logo.png"  width="10%" margin-right="12px" /></a>
       
            <div id="mainListDiv" class="main_list">
                <ul class="navlinks">
                    <li><a href="\Accuiel" >ACCUEIL </a></li>
                    <li><a href="#about"> À PROPOS</a></li>
                    <li><a href="#team" >BUREAU</a></li>
                    <li><a href="https://neurologieatn.000webhostapp.com/index.html" class="anime">26<sup>éme</sup> CONGRÈS</a></li>
                    <li><a href="#portfolio">GALERIE </a></li>
                    <li><a href="#services">LIENS UTILES</a></li>
                    <li><a href="#contact">CONTACTE </a></li>
                </ul>
            </div>
          
        </div>
    </nav>
 <h3>
</h3>

    <a class="ca3-scroll-down-link ca3-scroll-down-arrow" data-ca3_iconfont="ETmodules" data-ca3_icon="" href="#about"></a>
    
    <section class="home">
    </section>
    
    <div style="height: 1000px">
        <!-- just to make scrolling effect possible -->
            <h2 class="myH2"> </h2>
            <div id="polina">
<a href="https://neurologieatn.000webhostapp.com/index.html"></a><h1>Présentation de l'Association Tunisienne de Neurologie</h1></a>

			<!-- <p class="myP">This is a responsive fixed navbar animated on scroll</p>
			<p class="myP">I took inspiration from  ABDO STEIF (<a href="https://codepen.io/abdosteif/pen/bRoyMb?editors=1100">https://codepen.io/abdosteif/pen/bRoyMb?editors=1100</a>)
			and Dicson <a href="https://codepen.io/dicson/pen/waKPgQ">(https://codepen.io/dicson/pen/waKPgQ)</a></p>
			<p class="myP">I HOPE YOU FIND THIS USEFULL</p>
			<p class="myP">Albi</p>--> 
			
                <div class="container" id="about">
  <div class="accordion-option">
    <h3 class="title"></h3>
    <a href="javascript:void(0)" class="toggle-accordion active" accordion-id="#accordion"></a>
  </div>
  <div class="clearfix"></div>
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
        L'Association Tunisienne de Neurologie
        </a>
      </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
        <p class="myP"> <strong> L'Association Tunisienne de Neurologie  </strong>est une société scientifique à but non lucratif, visant à promouvoir  <strong> la Neurologie clinique et les Neurosciences en Tunisie </strong>.
               Nous organisons des réunions locales, des conférences nationales et régionales et participons
                à la formation des jeunes Neurologues dès la période de Résidanat. </p>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingTwo">
        <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
        Nos objectifs
        </a>
      </h4>
      </div>
      <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
        <div class="panel-body">
        <p><strong> 1. Promouvoir la qualité et l'égalité des soins </strong> prodigués  aux patients souffrant de pathologies neurologiques dans les pays du Maghreb.</p>
                  <p><strong> 2. Soutenir tous les professionnels de la santé </strong> participant à la prise en charge de pathologies neurologiques.</p>
                  <p> <strong>3. Participer à l'élaboration de recommandations</strong> pour une prise en charge optimale des troubles neurologiques.</p>
                  <p> <strong>4. Proposer des orientations </strong> sur l'ensemble des questions en rapport avec la Neurologie clinique, y compris les questions éthiques et l'évaluation de la qualité des soins.</p>
                  <p><strong>5. Soutenir activement la recherche clinique </strong> en Tunisie</p>
                  <p> <strong>6.  Assurer la promotion et le suivi </strong> de l'apport de la ATN aux  bases de données scientifiques/ registres tunisiens pour les troubles neurologiques rares.</p>
                  <p> <strong> 7.  Participer au développement et à la pérennisation des audits</strong>  de la pratique neurologique en Tunisie.</p>
                  <p><strong> 8. Appuyer les Comités pédagogiques</strong>  dans les activités d'ensignement clinique en Neurologie.</p>
                  <p><strong> 9. Assurer le lien avec le Comité scientifique </strong> lorsque cela est nécessaire.</p>
                  <p><strong>10.  Proposer des résumés/commentaires </strong> de colloques scientifiques et d'autres conférences et séances de formation.</p>       
       
       
       
       
                </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingThree">
        <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
        26<sup>éme</sup> congrès national de neurologie
        </a>
      </h4>
      </div>
      <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
        <div class="panel-body">
         l'ATN vous propose un congrès national virtuel offrant des présentations sur les principales neuropathologies, tout au long de la vie et avec des intervenants comptant parmi les grands experts nationaux et internationaux.
        </div>
      </div>
    </div>
  </div>
</div>
    </div>

 

    
    <!-- Team  -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
<section class="our-team" id="team" id="zina">
    <div class="container">
        <div class="section-header">
            <h2 class="dark-text">Bureau Exécutif </h2>
            <div class="section-legend"> </div>
        </div>
        <hr />
        <div class="row" data-scrollreveal="enter left after 0s over 0.1s" data-sr-init="true" data-sr-complete="true">
            <div class="col-lg-3 col-sm-3 team-box">
                <div class="team-member">
                    <figure class="profile-pic">
                        <img src="http://localhost:8000/storage/team/team-2.jpg" alt="Uploaded image">
                    </figure>


                    <div class="member-details">


                        <h3 class="dark-text red-border-bottom">Dr Chokri MHIRI</h3>




                        <div class="position">Présiden</div>


                    </div>

                </div>

            </div>
<br>

            <div class="col-lg-3 col-sm-3 team-box">

                <div class="team-member">


                    <figure class="profile-pic">

                        <img src="http://localhost:8000/storage/team/team-1.jpg" alt="Uploaded image">

                    </figure>


                    <div class="member-details">


                        <h3 class="dark-text red-border-bottom">Dr Riadh GOUIDER</h3>




                        <div class="position">Past-Président</div>


                    </div>

                </div>

            </div>
           


            <div class="col-lg-3 col-sm-3 team-box">

                <div class="team-member">


                    <figure class="profile-pic">

                        <img src="http://localhost:8000/storage/team/team-3.jpg" alt="Uploaded image">

                    </figure>


                    <div class="member-details">


                        <h3 class="dark-text red-border-bottom">Dr Lobna MAALEJ</h3>




                        <div class="position">Vice Président</div>


                    </div>

                </div>

            </div>
            <div class="col-lg-3 col-sm-3 team-box">

<div class="team-member">


    <figure class="profile-pic">

        <img src="http://localhost:8000/storage/team/team-4.jpg" alt="Uploaded image">

    </figure>


    <div class="member-details">


        <h3 class="dark-text red-border-bottom">Dr Amina GARGOURI</h3>




        <div class="position"> Secrétaire Général </div>


    </div>

</div>

</div>
<br>
<div class="col-lg-3 col-sm-3 team-box">

<div class="team-member">


    <figure class="profile-pic">

        <img src="http://localhost:8000/storage/team/team-5.jpg" alt="Uploaded image">

    </figure>


    <div class="member-details">


        <h3 class="dark-text red-border-bottom">Dr Nadia BEN ALI</h3>




        <div class="position">Trésorier </div>


    </div>

</div>

</div>

            <div class="col-lg-3 col-sm-3 team-box">

                <div class="team-member">


                    <figure class="profile-pic">

                        <img src="http://localhost:8000/storage/team/team-6.jpg" alt="Uploaded image">

                    </figure>


                    <div class="member-details">


                        <h3 class="dark-text red-border-bottom">Dr Sana BEN AMOR</h3>




                        <div class="position">Secrétaire Général Adjoint</div>


                    </div>

    

                </div>

            </div>
          
          <div class="col-lg-3 col-sm-3 team-box">

                <div class="team-member">


                    <figure class="profile-pic">

                        <img src="http://localhost:8000/storage/team/team-7.jpg" alt="Uploaded image">

                    </figure>


                    <div class="member-details">


                        <h3 class="dark-text red-border-bottom">Dr Ines BEN ARBIA</h3>




                        <div class="position">Trésorier Adjoint </div>


                    </div>

                

                </div>

            </div>

        </div>
    </div>
</section>
    <!-- End Team  -->
   


    <!-- Galery  -->
<h1>Galery </h1>
    <div id="photos" id="portfolio">
  <ul id="photo-gallery">
    <li>
      <a href="/storage/portfolio/portfolio-1.jpg">
      <img src="/storage/portfolio/portfolio-1.jpg" alt="">
      </a>
    </li>
    <li>
      <a href="/storage/portfolio/portfolio-2.jpg">
      <img src="/storage/portfolio/portfolio-2.jpg"   alt="">      </a>
    </li>
    <li>
      <a href="/storage/portfolio/portfolio-3.jpg">
      <img src="/storage/portfolio/portfolio-3.jpg"   alt="">
      </a>
    </li>
    <li>
      <a href="/storage/portfolio/portfolio-4.jpg" >
      <img src="/storage/portfolio/portfolio-4.jpg"   alt="">
      </a>
    </li>
    <li>
      <a href="/storage/portfolio/portfolio-5.jpg">
      <img src="/storage/portfolio/portfolio-5.jpg"  alt="">
    </li>
    <li>
      <a href="/storage/portfolio/portfolio-10.jpg">
      <img src="/storage/portfolio/portfolio-10.jpg" class="img-fluid" alt="">
      </a>
    </li>
    <li>
      <a href="/storage/portfolio/portfolio-7.jpg">
      <img src="/storage/portfolio/portfolio-7.jpg" alt="">
      </a>
    </li>
    <li>
      <a href="/storage/portfolio/portfolio-8.jpg">
      <img src="/storage/portfolio/portfolio-8.jpg" alt="">
      </a>
    </li>
    <li>
      <a href="/storage/portfolio/portfolio-9.jpg" ">
      <img src="/storage/portfolio/portfolio-9.jpg"   alt="">      </a>
    </li>
  </ul>
</div>
    <!--  fin galery  -->




    <br>
    <br>
    <H1 id="services">Lien Utiles </H1>

 
  
   <section class="sec-boxes" role="section" >
      <adrticle class="box">
      
        <h1>PAANS</h1>
        <p> l'Association Panafricaine </br>des Sciences Neurologiques </p>
        <img src="http://localhost:8000/storage/img/paans.png" alt="logo PAANS" width="150px" ><br>

        <a href="http://www.paans.org/">  <button class="button" type="button" role="button" value="MORE">PAANS</button></a>

      </adrticle>
      <adrticle class="box">
            
        <h1>EAN </h1>
        <p> European Academy of Neurology </p>
        <img src="http://localhost:8000/storage/img/ean.jpg" alt="logo PANUS" width="99px" >
        <br>
        <a href="https://www.ean.org/"><button class="button" type="button" role="button" value="MORE">EAN</button></a>
      </adrticle>
      <adrticle class="box">
        <h1>WFN </h1>
        <p> Fédération Mondiale de Neurologie</p>
        <img src="storage/img/wfn.png" alt="logo WFN" width="260px" class ="pr-5 mr-3 mb-3" >

        <br>
        <a href="https://wfneurology.org/">  <button class="button" type="button" role="button" value="MORE">WFN</button></a>
      </adrticle>
      <adrticle class="box">
        <h1>PAUNS </h1>
        <p>l'Union Arabe des Sociétés de Neurologie </p>  
        <img src="storage/img/logo1.png" alt="logo PANUS" width="140px"> <br>
        <a href="http://pauns.net/">   <button class="button" type="button" role="button" value="MORE">PAUNS</button></a>
      </adrticle>
    </section>





    <br>
    <br>
    <br>
    <section id="contact" class="contact section-bg">
      <div class="container">

        <div class="section-title" data-aos="fade-in" data-aos-delay="100">
          <h2> Contact </h2>
        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="100">
          <div class="col-lg-6">
            <div class="info-box mb-4">
               <h3> Address</h3>
              <p>Appartement n°C07, 1er étage. Immeuble C. Résidence Tunis Carthage. Avenue 14 Janvier, route régionale n°21-Ariana 2083 Tunisie </p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="info-box  mb-4">
              <h3>Email </h3>
              <p>stneuro@yahoo.fr</p>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="info-box  mb-4">
              <h3>Numéros Utiles</h3>
                <p>Mobile: +216 93 179 678</p>
                <p> Fax   : +216 71 601 300</p>
            </div>
          </div>

        </div>

        <div class="row" data-aos="fade-up" data-aos-delay="200">

          <div class="col-lg-6 ">
          </div>

          <div class="col-lg-6">
            <form action="{{ url('Accuiel') }}" method="post" role="form" class="php-email-form">
            {{ csrf_field() }}
              <div class="form-row"> 
                <div class="col-md-6 form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Votre Nom" data-rule="minlen:4" data-msg="Veuillez saisir au moins 3 caractères" />
                  <div class="validate"></div>
                </div>
                <div class="col-md-6 form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Votre Email" data-rule="email" data-msg="Veuillez saisir un e-mail valide" />
                  <div class="validate"></div>
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Objet de Votre email" data-rule="minlen:4" data-msg="Veuillez saisir au moins 8 caractères" />
                <div class="validate"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" id="message" rows="5" data-rule="required" data-msg="Veuillez saisir votre message" placeholder="Message"></textarea>
                <div class="validate"></div>
              </div>
              <div class="mb-3">
                <div class="loading">Chargement</div>
                
                <div class="sent-message">Votre message a été envoyé. Merci!</div>
              </div>
              <div class="text-center"><button type="submit">Envoyer le message</button></div>
            </form>
          </div>

        </div>

      </div>
    </section>

		  <!-- Site footer 
          <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <h6>À-PROPOS </h6>
            <p class="text-justify">
            Cette année, l'ATN vous propose un congrès national virtuel offrant des présentations sur les principales neuropathologies, tout au long de la vie et avec des intervenants comptant parmi les grands experts nationaux et internationaux.          </div>

          <div class="col-xs-6 col-md-3" id="lien">
            <h6>LIEN UTILE</h6>
            <ul class="footer-links">
              <li><a href="http://www.pauns.net/index.php"> PAUNS </a></li>
              <li><a href="https://wfneurology.org/"> WFN  </a></li>
              <li><a href="http://www.paans.org/"> PAANS </a></li>
              <li><a href="https://www.ean.org/">EAN</a></li>
            </ul>
          </div>

          <div class="col-xs-6 col-md-3">
            <h6>Quick Links</h6>
            <ul class="footer-links">
              <li><a href="http://scanfcode.com/about/">About Us</a></li>
              <li><a href="http://scanfcode.com/contact/">Contact Us</a></li>
              <li><a href="http://scanfcode.com/contribute-at-scanfcode/">Contribute</a></li>
              <li><a href="http://scanfcode.com/privacy-policy/">Privacy Policy</a></li>
              <li><a href="http://scanfcode.com/sitemap/">Sitemap</a></li>
            </ul>
          </div>
        </div>
        <hr>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-12">
            <p class="copyright-text">Copyright &copy; 2017 All Rights Reserved by 
         <a href="#">Scanfcode</a>.
            </p>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12">
            <ul class="social-icons">
              <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
              <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
              <li><a class="dribbble" href="#"><i class="fa fa-dribbble"></i></a></li>
              <li><a class="linkedin" href="#"><i class="fa fa-linkedin"></i></a></li>   
            </ul>
          </div>
        </div>
      </div>
</footer> -->
    <!-- End Contact  -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- Jquery needed -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="js/scripts.js"></script>

<!-- Function used to shrink nav bar removing paddings and adding black background -->
    <script>
        $(window).scroll(function() {
            if ($(document).scrollTop() > 50) {
                $('.nav').addClass('affix');
                console.log("OK");
            } else {
                $('.nav').removeClass('affix');
            }
        });
        $('.navTrigger').click(function () {
    $(this).toggleClass('active');
    console.log("Clicked menu");
    $("#mainListDiv").toggleClass("show_list");
    $("#mainListDiv").fadeIn();

});
// lien utiles 
// Trigger CSS animations on scroll.
// Detailed explanation can be found at http://www.bram.us/2013/11/20/scroll-animations/

// Looking for a version that also reverses the animation when
// elements scroll below the fold again?
// --> Check https://codepen.io/bramus/pen/vKpjNP

jQuery(function($) {
  
  // Function which adds the 'animated' class to any '.animatable' in view
  var doAnimations = function() {
    
    // Calc current offset and get all animatables
    var offset = $(window).scrollTop() + $(window).height(),
        $animatables = $('.animatable');
    
    // Unbind scroll handler if we have no animatables
    if ($animatables.length == 0) {
      $(window).off('scroll', doAnimations);
    }
    
    // Check all animatables and animate them if necessary
		$animatables.each(function(i) {
       var $animatable = $(this);
			if (($animatable.offset().top + $animatable.height() - 20) < offset) {
        $animatable.removeClass('animatable').addClass('animated');
			}
    });

	};
  
  // Hook doAnimations on scroll, and trigger a scroll
	$(window).on('scroll', doAnimations);
  $(window).trigger('scroll');

});
$(document).ready(function() {

$(".toggle-accordion").on("click", function() {
  var accordionId = $(this).attr("accordion-id"),
    numPanelOpen = $(accordionId + ' .collapse.in').length;
  
  $(this).toggleClass("active");

  if (numPanelOpen == 0) {
    openAllPanels(accordionId);
  } else {
    closeAllPanels(accordionId);
  }
})

openAllPanels = function(aId) {
  console.log("setAllPanelOpen");
  $(aId + ' .panel-collapse:not(".in")').collapse('show');
}
closeAllPanels = function(aId) {
  console.log("setAllPanelclose");
  $(aId + ' .panel-collapse.in').collapse('hide');
}
   
});
//$('.hello-select').on('click',function(){ $(this).toggleClass('active'); });

// Form Placeholders
	
var colorHolder = new Array();
	var defaultTextHolder = new Array();
	var defaultColorHolder = new Array();
	
	function bravePlaceholder() {
		$('.add-placeholder').each(function(e){
			var id = $(this).attr('id');
			defaultColorHolder[id] = $(this).css('color');
			defaultTextHolder[id] = $(this).attr('title');
			colorHolder[id] = $(this).attr('phc');
			var placeholder = $(this).attr('title');
			$(this).val(placeholder);
			$(this).css('color',colorHolder[id]);
			
		});
	}
	bravePlaceholder();
	
	$('.add-placeholder').on('focus', function() {
		var id = $(this).attr('id');
		if ($(this).val() == defaultTextHolder[id])
		{
			$(this).val('');
			$(this).css('color',defaultColorHolder[id]);
		}
	});
	$('.add-placeholder').on('blur', function() {
		var id = $(this).attr('id');
		if ($(this).val() == '')
		{
			$(this).css('color',colorHolder[id]);
			$(this).val(defaultTextHolder[id]);
		}
	});
	
	// End Placeholders

// Hello Select
		$('.hello-select label').click(function() {
		var learnSelect = $(this).parent();
		var menu = learnSelect.find('.select-box');
		if (learnSelect.hasClass('active')) {
			learnSelect.removeClass('active');
			menu.fadeOut(300);
		}
		else {
			$('.select-box').fadeOut(300);
			$('.hello-select').removeClass('active');
			learnSelect.addClass('active');
			menu.fadeIn(300);
		}
	});
	
	$('body').click(function(event) {
		if (!$(event.target).closest('.hello-select').length) {
			$('.select-box').fadeOut(300);
			$('.hello-select').removeClass('active');
		}
	});
	
	$('.select-box a').on('click',function() {
		var selected_value = $(this).text();
		var pass_value = $(this).attr('data-filter-value');
		$('#hello-updates').val(pass_value);
		var learn_select = $(this).parent().parent();
		var select_box = $(this).parent();
		if (!($(this).hasClass('default-select'))) {
			learn_select.addClass('selected-active').removeClass('active');
			select_box.find('a').removeClass('selected');
			$(this).addClass('selected');
		}
		else {
			select_box.find('a').removeClass('selected');
			learn_select.removeClass('selected-active').removeClass('active');
			selected_value = '';
		}
		
		learn_select.find('.select-box').hide();
		learn_select.find('.selected-value').text(selected_value);
	});
	
	// Form
	
	function std_email_form(obj,message) {
		var hasError = false;
		obj.find('.required').each(function() {
			if (jQuery.trim($(this).val()) == '' || $(this).val() == defaultTextHolder[$(this).attr('id')] || $(this).val() === '0') {
				hasError = true;
				$(this).removeClass('valid').addClass('invalid');
			}
			else {
				$(this).addClass('valid').removeClass('invalid');
			}
		});
		
		var sending = obj.find('.submit-sending');
		var invalid = obj.find('.submit-invalid');
			invalid.hide();
		var sendBtn = obj.find('.submit-btn');
		sendBtn.hide();
		sending.css('display','inline');
		
		if(!hasError) {
			invalid.hide();
			var formInput = obj.serialize();
			$.post(obj.attr('action'),formInput, function(data){
				var output = $.parseJSON(data);
				if (output.result === 'success') {
					sending.hide();
					obj.before(message);
					obj.hide();
				}
				else {
					sending.hide();
					invalid.html('Please provide a valid email address and all required fields').fadeIn(200);
					sendBtn.show();
				}
			});
		}
		else {
			sending.hide();
			invalid.hide().html('Please fill in the required fields').fadeIn(200);
			sendBtn.show();
		}
		
		return false;
	}
	$('.select-box a').click(function(e) {
		e.preventDefault();
	});
	
	$('#hello-form').submit(function(e) {
		e.preventDefault();
		std_email_form($(this),'<div class="success-message"><p>Thank you for your message! <br />Someone from our team will respond to you as quickly as possible.</p></div>');
	});
	
	$('#hello-clear').click(function(e) {
		var obj = $('#hello-form');
		obj.find('.required').each(function() {
			$(this).val('');
			if ($(this).hasClass('get-updates')) {
				$(this).val('yes');
			}
		});
		$('.hello-select').removeClass('selected-active');
		$('.select-box a').removeClass('selected');
		bravePlaceholder();
	});
	
	$('#back-to-top').click(function() {
		htmlbody.animate({scrollTop: 0}, 900, 'easeInOutQuint');
		return false;
	});

  
var $overlay = $('<div id="overlay"></div>');
var $image = $("<img>");

//An image to overlay
$overlay.append($image);

//Add overlay

h
$("body").append($overlay);

  //click the image and a scaled version of the full size image will appear
  $("#photo-gallery a").click( function(event) {
    event.preventDefault();
    var imageLocation = $(this).attr("href");

    //update overlay with the image linked in the link
    $image.attr("src", imageLocation);

    //show the overlay
    $overlay.show();
  } );

  $("#overlay").click(function() {
    $( "#overlay" ).hide();
  });
  const persons = [
  {
    name: "Amber Gibs",
    photo: "https://serving.photos.photobox.com/53464066dc132288128cb11531bb767f0082dcde75f3b845cd905d38c4223e95e0dc7bc9.jpg",
    title: "Developer",
    bio:
      "<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet architecto ea blanditiis quo labore esse magnam illum ut quibusdam. Corrupti ratione iure aliquam adipisci! Harum vitae laboriosam temporibus illo suscipit?</p><p>Saepe repudiandae rerum quam ut perferendis, ullam similique nemo quod, assumenda mollitia consectetur. Eveniet optio maxime perferendis odit possimus? Facilis architecto nesciunt doloribus consectetur culpa veritatis accusamus expedita quos voluptate!</p><p>Nisi provident minus possimus optio voluptate rem, perspiciatis, placeat, culpa aperiam quod temporibus.</p>",
    social: {
      facebook: "#",
      twitter: "https://twitter.com/knyttneve",
      linkedin: "#"
    }
  },
  {
    name: "Carl Roland",
    photo: "https://serving.photos.photobox.com/2226093445b640ea69b3247d4e4a31ee16d7569a38f898affce33adc8fc8d0f3ecf79591.jpg",
    title: "Developer",
    bio:
      "<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet architecto ea blanditiis quo labore esse magnam illum ut quibusdam. Corrupti ratione iure aliquam adipisci! Harum vitae laboriosam temporibus illo suscipit?</p><p>Saepe repudiandae rerum quam ut perferendis, ullam similique nemo quod, assumenda mollitia consectetur. Eveniet optio maxime perferendis odit possimus? Facilis architecto nesciunt doloribus consectetur culpa veritatis accusamus expedita quos voluptate!</p><p>Nisi provident minus possimus optio voluptate rem, perspiciatis, placeat, culpa aperiam quod temporibus.</p>",
    social: {
      facebook: "#",
      twitter: "https://twitter.com/knyttneve",
      linkedin: "#"
    }
  },
  {
    name: "Paul Wilson",
    photo: "https://serving.photos.photobox.com/80553616a29779bd113f7a340b9b1eae3636dd38534c57877597a17b7131be741c67b3ae.jpg",
    title: "UI Designer",
    bio:
      "<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet architecto ea blanditiis quo labore esse magnam illum ut quibusdam. Corrupti ratione iure aliquam adipisci! Harum vitae laboriosam temporibus illo suscipit?</p><p>Saepe repudiandae rerum quam ut perferendis, ullam similique nemo quod, assumenda mollitia consectetur. Eveniet optio maxime perferendis odit possimus? Facilis architecto nesciunt doloribus consectetur culpa veritatis accusamus expedita quos voluptate!</p><p>Nisi provident minus possimus optio voluptate rem, perspiciatis, placeat, culpa aperiam quod temporibus.</p>",
    social: {
      facebook: "#",
      twitter: "https://twitter.com/knyttneve",
      linkedin: "#"
    }
  },
  {
    name: "Alice Jenkins",
    photo: "https://serving.photos.photobox.com/52898788b03c8a0e32a8cb52b4d43d7525f119daa2629569dbda0a8d827192217fb64c22.jpg",
    title: "QA Engineer",
    bio:
      "<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet architecto ea blanditiis quo labore esse magnam illum ut quibusdam. Corrupti ratione iure aliquam adipisci! Harum vitae laboriosam temporibus illo suscipit?</p><p>Saepe repudiandae rerum quam ut perferendis, ullam similique nemo quod, assumenda mollitia consectetur. Eveniet optio maxime perferendis odit possimus? Facilis architecto nesciunt doloribus consectetur culpa veritatis accusamus expedita quos voluptate!</p><p>Nisi provident minus possimus optio voluptate rem, perspiciatis, placeat, culpa aperiam quod temporibus.</p>",
    social: {
      facebook: "#",
      twitter: "https://twitter.com/knyttneve",
      linkedin: "#"
    }
  }
];

const app = new Vue({
  el: "#app",
  data() {
    return {
      persons: persons,
      selectedPersonIndex: null,
      isSelected: false,
      selectedPerson: null,
      inlineStyles: null,
      isReady: false,
      isOk: false,
      selectedPersonData: {
        name: null,
        title: null,
        photo: null,
        social: {
          facebook: null,
          twitter: null,
          linkedin: null
        }
      }
    };
  },
  methods: {
    selectPerson(index, el) {
      if (!this.isOk) {
        this.selectedPersonIndex = index;
        this.isSelected = true;
        el.target.parentElement.className == "person-details"
          ? (this.selectedPerson = el.target.parentElement.parentElement)
          : (this.selectedPerson = el.target.parentElement);

        this.selectedPerson.classList.add("person-selected");
        this.selectedPerson.setAttribute(
          "style",
          `width:${this.selectedPerson.offsetWidth}px;`
        );
        this.selectedPersonData = this.persons[index];
        window.setTimeout(() => {
          this.inlineStyles = `width:${this.selectedPerson
            .offsetWidth}px;height:${this.selectedPerson
            .offsetHeight}px;left:${this.selectedPerson.offsetLeft}px;top:${this
            .selectedPerson.offsetTop}px;position:fixed`;
          this.selectedPerson.setAttribute("style", this.inlineStyles);
        }, 400);
        window.setTimeout(() => {
          this.isReady = true;
          this.isOk = true;
        }, 420);
      } else {
        this.reset();
      }
    },
    reset() {
      this.isReady = false;
      window.setTimeout(() => {
        this.selectedPerson.classList.add("person-back");
      }, 280);
      window.setTimeout(() => {
        this.selectedPerson.setAttribute("style", "");
      }, 340);
      window.setTimeout(() => {
        this.isSelected = false;
        this.selectedPerson.classList.remove("person-back", "person-selected");
        this.isOk = false;
      }, 400);
    }
  }
});


    </script>
    
</body>
</html>