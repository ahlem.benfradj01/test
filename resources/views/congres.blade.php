<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>26 ème Congré National de
    NEUROLOGIE
  </title>
  <!-- MDB icon -->
  <link rel="icon" href="img/stn.png" type="image/x-icon" class="fas fa-camera fa-3x">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
  <!-- Google Fonts Roboto -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <!-- Material Design Bootstrap -->
  <link rel="stylesheet" href="css/mdb.min.css">
  <!-- Your custom styles (optional) -->
  <link rel="stylesheet" href="css/style.css">
</head>

<body>

  <header>
    <!-- navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar" >
      <div class="container">
        <a class="navbar-brand" href="#"><strong>ATN</strong></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7"
          aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Accueil <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#programme">Programme</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#spons">Sponsors</a>
            </li>
          </ul>
          <form class="form-inline">
            <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
          </form>
        </div>
      </div>
    </nav>
    <!-- Carousel wrapper -->
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block w-100" src="img/flyer2.jpg">
          <div class="centered">26 <sup>éme</sup> Congrés National de <br> <span class="nero">NEUROLOGIE</span></div>
        </div>

      </div>
    </div>
  </header>

  <!--Accordion wrapper-->
  <div class="accordion md-accordion accordion-3 z-depth-1-half" id="accordionEx194" role="tablist"
    aria-multiselectable="true" id="programme">



    <h2 class="text-center text-uppercase black-text py-4 px-3">Programme</h2>

    <hr class="mb-0">

    <!-- Accordion card -->
    <div class="card">

      <!-- Card header -->
      <div class="card-header" role="tab" id="heading4">
        <a data-toggle="collapse" data-parent="#accordionEx194" href="#collapse4" aria-expanded="true"
          aria-controls="collapse4">
          <h3 class="mb-0 red-text">
            Jeudi Après-Midi – 10 Décembre 2020<div class="animated-icon1 float-right mt-1">
              <span></span><span></span><span></span></div>
          </h3>
        </a>
      </div>

      <!-- Card body -->
      <div id="collapse4" class="collapse show" role="tabpanel" aria-labelledby="heading4"
        data-parent="#accordionEx194">
        <div class="card-body pt-0">

          <h3 class="pos-title"> 15h45-16h00: Ouverture Officielle du congrès </h3>


          <h4> 16h00 – 16h40: Session – Epilepsie </h4>


          <p>16h00 - 16h20: Voyage à travers les épilepsies génétiques de l’enfant
            I. Turki</p>

          <p>16h20 - 16h40: Conduite à tenir devant une première crise épileptique
            M. Djellaoui, H. Leklou (Algérie)</p>


          <h4>16h40 - 17h10: Symposium HIKMA</h4>


          <h4 class="pos-caf">17h10 - 17h30: Intermède & Visite virtuelle des stands</h4>


          <h4>17h30 - 18h30: PanArab Session – Complicated Epilepsy (English session)</h4>


          <p> 17h30 -17h50: Pharmacoresistant Epilepsy N. Ben Ali (Tunisie) </p>

          <p>17h50 - 18h10: Sudden unexpected death in Epilepsy R. Al-Baradie (KSA)</p>

          <p>18h10 -18h30: Epilepsy in elderly H. Hosny (Egypte)</p>
        </div>
      </div>
    </div>
    <!-- Accordion card -->

    <!-- Accordion card -->
    <div class="card">

      <!-- Card header -->
      <div class="card-header" role="tab" id="heading5">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx194" href="#collapse5"
          aria-expanded="false" aria-controls="collapse5">
          <h3 class="mb-0 red-text">
            Vendredi Matin – 11 décembre 2020 <div class="animated-icon1 float-right mt-1">
              <span></span><span></span><span></span></div>
          </h3>
        </a>
      </div>

      <!-- Card body -->
      <div id="collapse5" class="collapse" role="tabpanel" aria-labelledby="heading5" data-parent="#accordionEx194">
        <div class="card-body pt-0">

          <h4> 10h30 – 11h10: Session – Neurology through Ages </h4>


          <p>10h30 – 10h50 : Spectre Clinique et profil étiologique de la déficience intellectuelle en Algérie
            A.Saadi, L. Ali Pacha (Algérie)</p>

          <p>10h50 – 11h10 : Syndromes démyélinisants acquis de l’enfant : Quelle démarche ?
            H. Ben Rhouma (Tunisie)</p>


          <h4>11h10 - 11h40: Symposium ROCHE</h4>
          <h4 class="pos-caf">11h40 - 12h00: Intermède & Visite virtuelle des stands </h4>

          <h4>12h00 - 13h00: Session – Neurology through Ages</h4>


          <p>12h00 – 12h20: Maladie de Parkinson : l’âge de début change tout M. Ben Djebara (Tunisie) </p>


          <p> 12h20 – 12h40: Crises psychogènes non Epileptiques B. Ben Amou (Tunisie) </p>

          <p>12h40 – 13h00: L’épilepsie dans les démences chez le sujet âgé
            S. Belarbi, S. Mokrane Makri (Algérie)</p>
        </div>
      </div>
    </div>
    <!-- Accordion card -->

    <!-- Accordion card -->
    <div class="card">

      <!-- Card header -->
      <div class="card-header" role="tab" id="heading6">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx194" href="#collapse6"
          aria-expanded="false" aria-controls="collapse6">
          <h3 class="mb-0 red-text">
            Vendredi Après-Midi – 11 décembre 2020 <div class="animated-icon1 float-right mt-1">
              <span></span><span></span><span></span></div>
          </h3>
        </a>
      </div>

      <!-- Card body -->
      <div id="collapse6" class="collapse" role="tabpanel" aria-labelledby="heading6" data-parent="#accordionEx194">
        <div class="card-body pt-0">
          <h4> 16h30 - 17h30: SEP - Highlights 2020 </h4>


          <p>16h30 - 16h50: Immunopathologie de la SEP
            M. Ben Abdeljelil (Maroc)
          </p>

          <p>16h50 - 17h10: Guidelines dans le traitement de la SEP et escalade thérapeutique
            N.Grigoriadis (Grèce)
          </p>

          <p>17h10 - 17h30: SEP: Real life!
            R. Gouider (Tunisie)
          </p>

          <h4 class="pos-caf">17h30 - 17h50: Intermède & Visite Virtuelle des stands</h4>
          <h4>17h50 - 18h40: Session – Maladies de la Substance Blanche </h4>




          <p>17h50 - 18h10: Les Mogopathies
            Y. Mebrouk (Maroc)
          </p>


          <p> 18h10 - 18h30: Eléments pronostiques de la SEP progressive
            A. Tourbah (France)
          </p>

          <p>18h30 - 18h40: Discussion</p>
          <h4>18h40 - 19h30: Symposium Merck </h4>

        </div>
      </div>
    </div>
    <!-- Accordion card -->

    <!-- Accordion card -->
    <div class="card">

      <!-- Card header -->
      <div class="card-header" role="tab" id="heading7">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx194" href="#collapse6"
          aria-expanded="false" aria-controls="collapse6">
          <h3 class="mb-0 red-text">
            Samedi Matin – 12 Décembre 2020 <div class="animated-icon1 float-right mt-1">
              <span></span><span></span><span></span></div>
          </h3>
        </a>
      </div>

      <!-- Card body -->
      <div id="collapse6" class="collapse" role="tabpanel" aria-labelledby="heading6" data-parent="#accordionEx194">
        <div class="card-body pt-0">
          <h4>10h30- 11h10: Session – Neuro-COVID (I)</h4>


          <p>10h30 - 10h50: AVC et COVID-19
            S. Ben Amor (Tunisie)
          </p>

          <p>10h50 - 11h10: Les manifestations neurologiques de la COVID-19
            S. Belarbi, S. Mokrane-Makri (Algérie)
          </p>


          <h4>11h10 - 11h40: Symposium SANOFI</h4>
          <h4 class="pos-caf">11h40 - 12h00: Intermède & Visite Virtuelle des stands</h4>
          <h4>12h00 - 13h00: Session – Neuro-COVID (II)</h4>




          <p>12h00 - 12h20: Symptômes neurologiques de la COVID-19 : enquête Tunisienne
            I. Kacem (Tunisie)
          </p>


          <p> 12h20 - 12h40: Neuro COVID-19 : de quoi parle-t-on ?
            H. El Otmani (Maroc)
          </p>

          <p>12h40 - 13h00: Séquelles neurologiques post-Covid-19
            N. Bouzouaya (Tunisie)
          </p>

        </div>
      </div>
    </div>
    <!-- Accordion card -->


    <!-- Accordion card -->
    <div class="card">

      <!-- Card header -->
      <div class="card-header" role="tab" id="heading6">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx194" href="#collapse6"
          aria-expanded="false" aria-controls="collapse6">
          <h3 class="mb-0 red-text">
            Samedi Après-Midi – 12 Décembre 2020 <div class="animated-icon1 float-right mt-1">
              <span></span><span></span><span></span></div>
          </h3>
        </a>
      </div>

      <!-- Card body -->
      <div id="collapse6" class="collapse" role="tabpanel" aria-labelledby="heading6" data-parent="#accordionEx194">
        <div class="card-body pt-0">
          <h4>16h00 - 16h40: Session - Actualités diagnostiques & thérapeutiques (I)</h4>


          <p>16h00 - 16h20: Myélites
            H. Derbali (Tunisie)
          </p>

          <p>16h20 - 16h40: Migraine chronique
            M. Aissi (Tunisie)
          </p>


          <h4>16h40 - 17h30: Symposium BIOLOGIX</h4>
          <h4 class="pos-caf">17h30-18h00 : Intermède & Visite virtuelle des stands </h4>
          <h4>18h00 - 19h00 : Session - Actualités diagnostiques & thérapeutiques (II)</h4>




          <p>18h00 - 18h20: Thérapie génique dans les AMS
            M. Damak (Tunisie)
          </p>


          <p> 18h20 - 18h40: Quoi de neuf dans le syndrome des anticorps antiphospholipides pour le Neurologue ?
            S. Younes (Tunisie)
          </p>

          <p>18h40 - 19h00: Quoi de neuf en Neuro-Ophtalmologie ?
            A. Hassine (Tunisie)
          </p>
          <h4>19h00 Clôture du congrès</h4>
        </div>
      </div>
    </div>
    <!-- Accordion card -->
  </div>
  <!--/.Accordion wrapper-->



<div class="hm-gradient">
  <div class="full-bg-img">
    <div class="container flex-center">
      <div class="row pt-5 mt-3">
        <div class="col-lg-6 wow fadeIn mb-5 text-center text-lg-left">
          <div class="white-text">
            <img src="img/salle.jpg" class="img-sponsors" alt="Responsive ">
            <hr class="hr-light wow fadeInLeft" data-wow-delay="0.3s">
            <img src="img/poster.jpg" class="img-sponsors" alt="Responsive ">
           
          </div>
        </div>

        <div class="col-lg-6 wow fadeIn">
          <div class="embed-responsive embed-responsive-16by9 wow fadeInRight">
            <iframe class="embed-responsive-item" src="congre.mp4"
              allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


  <div id="spons">
    <div class="sponsors-pos">
      <p class="sponsors-title">Nos Sponsors</p>
      <p class="sponsors-text">"Nos chaleureux remerciements s'adressent aux Laboratoires pour tout leur soutien pour
        leur précieux travail!"</p>
      <img src="img/logo-labo.png" class="img-sponsors" alt="Responsive ">
    </div>
  </div>

  <footer class="bg-primary text-white text-center text-lg-left">
    <!-- Grid container -->
    <div class="container p-4">
      <!--Grid row-->
      <div class="row">
        <!--Grid column-->
        <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
          <h5 class="text-uppercase"> Bienvenue au 26 <sup>éme</sup> congrés National de Neurologie</h5>

          
        </div>
        <!--Grid column-->



        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase mb-0">Les informations</h5>

          <ul class="list-unstyled">
            <li>
              <a href="#!" class="text-white">Email :stnneuro@yahoo.com</a>
            </li>
          </br> 
            <li>
              <a href="#!" class="text-white">Téléphone : +216 93 179 678</a>
            </li>
            
          </ul>
        </div>
        <!--Grid column-->
      </div>
      <!--Grid row-->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
      © 2020 Copyright:
      <a class="text-white" href="https://mdbootstrap.com/">Développer par </a>
    </div>
    <!-- Copyright -->
  </footer>
  <!-- End your project here-->

  <!-- jQuery -->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <!-- Your custom scripts (optional) -->
  <script type="text/javascript" src="js/MyJS.js"></script>

</body>

</html>