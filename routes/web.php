<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/Accuiel', function () {
    return view('/Accuiel');
});
/*Route::get('/test-contact', function () {
    return new App\Mail\Contact();
});*/
Route::get('/test-contact','ContactController@store');

Route::get('/Accuiel/info', function () {
    return view('/Accuiel/info');
});
Route::post('/Accuiel','ContactController@store');

Route::get('/info', function () {
    return view('/info');
});
Route::get('/congres', function () {
    return view('/congres');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
